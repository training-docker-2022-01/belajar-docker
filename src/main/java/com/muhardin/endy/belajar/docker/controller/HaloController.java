package com.muhardin.endy.belajar.docker.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HaloController {
    @GetMapping("/info")
    public Map<String, String> info(HttpServletRequest request){
        Map<String, String> hasil = new HashMap<>();
        hasil.put("waktu", LocalDateTime.now().toString());
        hasil.put("ip", request.getLocalAddr().toString());
        hasil.put("hostname", request.getLocalName());
        return hasil;
    }
}
