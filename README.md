# Belajar Docker

Arsitektur Docker

[![Arsitektur Docker](img/arsitektur-docker.png)](img/arsitektur-docker.png)

Workflow Development dengan Docker

[![Workflow Docker](img/workflow-development-docker.png)](img/workflow-development-docker.png)

## Instalasi Docker di Windows ##

1. Pastikan fitur virtualization sudah diaktifkan di BIOS

    [![Virtualization BIOS](img/01-bios-hyperv.png)](img/01-bios-hyperv.png)

2. Aktifkan fitur virtualization di Windows Features

    [![Windows Features](img/02-windows-feature.png)](img/02-windows-feature.png)

    [![Windows Virtualization](img/03-enable-hyperv.png)](img/03-enable-hyperv.png)

3. Install WSL

    * Jalankan command prompt sebagai administrator
    * Install WSL di command prompt administrator

        ```
        wsl --install
        ```

4. Install docker desktop sebagai administrator



## Menjalankan Docker Container ##

1. Menjalankan MySQL 8 dengan root password `abcd1234`

    ```
    docker run --name belajar-mysql-8 -e MYSQL_ROOT_PASSWORD=abcd1234 mysql:8
    ```

2. Mapping port MySQL supaya bisa diakses di port `10000`

    ```
    docker run --name belajar-mysql-8 -p 10000:3306 -e MYSQL_ROOT_PASSWORD=abcd1234 mysql:8
    ```

3. Connect ke MySQL

    ```
    mysql --host=127.0.0.1 --port=10000 -u root -p
    ```


## Membuat Docker Image ##

1. [Instal dulu kelengkapan Java SDK (Java SDK 17, Maven 3)](https://software.endy.muhardin.com/java/persiapan-coding-java/)

2. Build project Java

    ```
    mvn clean package
    ```

3. Build docker image

    ```
    docker build -t belajar-docker .
    ```

4. Jalankan docker image yang baru saja di-build

    ```
    docker run -p 9090:8080 belajar-docker
    ```

5. Browse aplikasi di http://localhost:9090/info

## Troubleshooting ##

* Pesan error : `docker: Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?.
See 'docker run --help'.`

    - Penyebab : docker desktop belum dijalankan 

