FROM --platform=linux/amd64 eclipse-temurin:17-jdk-alpine
ADD target/belajar-docker-0.0.1-SNAPSHOT.jar /opt/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app.jar"]